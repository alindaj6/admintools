package com.example.anggasetiawan.admintools;


import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.anggasetiawan.admintools.Model.POJOMemberDetail.Datum;
import com.example.anggasetiawan.admintools.Model.POJOMemberDetail.Memberdetail;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MemberDetail extends AppCompatActivity {

    @BindView(R.id.memberCbTopUp)
    CheckBox memberTopup;

    @BindView(R.id.etPhoneMember)
    EditText phoneMember;

    @BindView(R.id.btnCheckMember)
    Button checkMember;

    @BindView(R.id.tvName)
    TextView name;

    @BindView(R.id.tvPhone)
    TextView phone;

    @BindView(R.id.tvEmail)
    TextView email;

    @BindView(R.id.tvAddress)
    TextView address;

    @BindView(R.id.tvRegisterDate)
    TextView registerDate;

    @BindView(R.id.tvDefaultLocker)
    TextView defaultLocker;

    @BindView(R.id.tvLatitude)
    TextView latitude;

    @BindView(R.id.tvLongitude)
    TextView longitude;

    @BindView(R.id.tvIdCardImage)
    TextView idCardImage;

    @BindView(R.id.tvIdCardStatus)
    TextView idCardStatus;

    private ProgressDialog dialog;
    private NetworkMethodRetrofit2 retrofit2 = new NetworkMethodRetrofit2();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_detail);
        ButterKnife.bind(this);

        checkMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (memberTopup.isChecked()){
                    getUSerDetail(CommonConstants.INTERNAL_API_TOKEN, CommonConstants.INTERNAL_URL, phoneMember.getText().toString().trim());
                } else if (!memberTopup.isChecked()){
                    getUSerDetail(CommonConstants.DEV_API_TOKEN, CommonConstants.DEV_URL, phoneMember.getText().toString().trim());
                }

            }
        });
    }

    private void getUSerDetail(String token, String url, String handphone) {

        showProgressDialog("Processing...");

        com.example.anggasetiawan.admintools.Body.MemberDetail memberDetail = new com.example.anggasetiawan.admintools.Body.MemberDetail(token, handphone);

        MemberDataAPI service = retrofit2.getRetrofit2(url).create(MemberDataAPI.class);

        Call<Memberdetail> call = service.getMemberDetail(memberDetail);

        call.enqueue(new Callback<Memberdetail>() {
            @Override
            public void onResponse(Call<Memberdetail> call, Response<Memberdetail> response) {

                if (response.body().getResponse().getCode() == 200 && response.body().getTotalPage() == 1) {
                    List<Datum> datumList = response.body().getData();
                    name.setText(datumList.get(0).getMemberName());
                    phone.setText(datumList.get(0).getPhone());
                    email.setText(datumList.get(0).getEmail());

                    if (datumList.get(0).getAddress() != null){
                        address.setText(String.valueOf(datumList.get(0).getAddress()));
                    }

                    registerDate.setText(datumList.get(0).getRegisterDate());

                    if (datumList.get(0).getDefaultLockerName() != null){
                        defaultLocker.setText(datumList.get(0).getDefaultLockerName());
                    }
                    if (datumList.get(0).getLatitude() != null){
                        latitude.setText(datumList.get(0).getLatitude());
                    }
                    if (datumList.get(0).getLongitude() != null){
                        longitude.setText(datumList.get(0).getLongitude());
                    }
                    if (datumList.get(0).getIdCardImage() != null){
                        idCardImage.setText(datumList.get(0).getIdCardImage());
                    }
                    if (datumList.get(0).getIdCardValidateStatus() != null){
                        idCardStatus.setText(String.valueOf(datumList.get(0).getIdCardValidateStatus()));
                    }

                } else {
                    name.setText(response.body().getResponse().getMessage());
                }
                dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<Memberdetail> call, Throwable t) {
                Log.d("PopBox", "UnAuthorized Access");
                dismissProgressDialog();
            }
        });
    }

    public void showProgressDialog(String message){
        if (dialog == null)
            dialog = new ProgressDialog(this);
        if (dialog.isShowing())
            updateProgressDialog(message);
        else {
            dialog.setMessage(message);
            dialog.setCancelable(false);
            dialog.show();
        }
    }

    public void updateProgressDialog(final String message){
        if (dialog.isShowing()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    dialog.setMessage(message);
                }
            });
        }
    }

    public void dismissProgressDialog(){
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
    }

    public void showMessage(String message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
