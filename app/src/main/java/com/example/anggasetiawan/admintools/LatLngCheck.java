package com.example.anggasetiawan.admintools;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.anggasetiawan.admintools.Adapter.Adapter;
import com.example.anggasetiawan.admintools.Body.LockerLocation;
import com.example.anggasetiawan.admintools.Model.POJOLockerLocation.Datum;
import com.example.anggasetiawan.admintools.Model.POJOLockerLocation.Lockerlocation;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LatLngCheck extends AppCompatActivity {

    @BindView(R.id.rvRecyclerLatLng) RecyclerView recyclerView;
    @BindView(R.id.btnCheck) Button check;
    @BindView(R.id.spCountry) Spinner spinnerCountry;
    @BindView(R.id.LatLngCbURL) CheckBox checkBoxURL;

    private String[] arraySpinner;
    private NetworkMethodRetrofit2 retrofit2 = new NetworkMethodRetrofit2();
    private List<Datum> lockerLocationList;
    private LinearLayoutManager mLinearLayoutManager;

    List<BrokenLatLng> brokenLatLngList;

    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lat_lng_check);
        ButterKnife.bind(this);

        brokenLatLngList = new ArrayList<BrokenLatLng>();

        mLinearLayoutManager = new LinearLayoutManager(this);

        this.arraySpinner = new String[] {"Indonesia", "Malaysia"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, arraySpinner);
        spinnerCountry.setAdapter(adapter);

        check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (checkBoxURL.isChecked()){
                    showAllLocker(CommonConstants.INTERNAL_API_TOKEN, CommonConstants.INTERNAL_URL, spinnerCountry.getSelectedItem().toString());
                } else if (!checkBoxURL.isChecked()){
                    showAllLocker(CommonConstants.DEV_API_TOKEN, CommonConstants.DEV_URL, spinnerCountry.getSelectedItem().toString());
                }
            }
        });
    }

    private void showAllLocker(String token, String url, String country){

        showProgressDialog("Processing...");

        //String token = "";
        //String url = "";

        //String country = "";
        String province = "";
        String city = "";

        final LockerLocation lockerLocation = new LockerLocation(token, country, province, city);

        LockerAPI service = retrofit2.getRetrofit2(url).create(LockerAPI.class);

        Call<Lockerlocation> call = service.getLockerLocation(lockerLocation);

        call.enqueue(new Callback<Lockerlocation>() {
            @Override
            public void onResponse(Call<Lockerlocation> call, Response<Lockerlocation> response) {

                if (response.body().getResponse().getCode().equals("200")) {
                    for (int i = 0; i < response.body().getData().size(); i++){
                        if (tesst(response.body().getData().get(i).getLatitude(), '.') || tesst(response.body().getData().get(i).getLongitude(), '.')){

                            BrokenLatLng brokenLatLng = new BrokenLatLng();
                            brokenLatLng.setId(response.body().getData().get(i).getId());
                            brokenLatLng.setName(response.body().getData().get(i).getName());
                            brokenLatLng.setAddress(response.body().getData().get(i).getAddress());
                            brokenLatLng.setAddress2(response.body().getData().get(i).getAddress2());
                            brokenLatLng.setZipCode(response.body().getData().get(i).getZipCode());
                            brokenLatLng.setLatitude(response.body().getData().get(i).getLatitude());
                            brokenLatLng.setLongitude(response.body().getData().get(i).getLongitude());
                            brokenLatLng.setCity(response.body().getData().get(i).getCity());
                            brokenLatLng.setProvince(response.body().getData().get(i).getProvince());
                            brokenLatLng.setCountry(response.body().getData().get(i).getCountry());
                            brokenLatLng.setOperationalHours(response.body().getData().get(i).getOperationalHours());
                            brokenLatLng.setLockerCapacity(response.body().getData().get(i).getLockerCapacity());
                            brokenLatLngList.add(brokenLatLng);
                        }
                    }

                    recyclerView.setHasFixedSize(true);
                    recyclerView.setLayoutManager(mLinearLayoutManager);
                    recyclerView.setAdapter(new Adapter(getApplicationContext(), brokenLatLngList));
                }
                dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<Lockerlocation> call, Throwable t) {
                dismissProgressDialog();
            }
        });
    }

    private Boolean tesst (String s, char letter){
        return s.indexOf(letter, s.indexOf(letter) + 1) > -1;
    }

    public void showProgressDialog(String message){
        if (dialog == null)
            dialog = new ProgressDialog(this);
        if (dialog.isShowing())
            updateProgressDialog(message);
        else {
            dialog.setMessage(message);
            dialog.setCancelable(false);
            dialog.show();
        }
    }

    public void updateProgressDialog(final String message){
        if (dialog.isShowing()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    dialog.setMessage(message);
                }
            });
        }
    }

    public void dismissProgressDialog(){
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
    }

    public void showMessage(String message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
