
package com.example.anggasetiawan.admintools.Model.POJOLockerLocation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Lockerlocation {

    @SerializedName("response")
    @Expose
    private Response response;
    @SerializedName("Filter")
    @Expose
    private Filter filter;
    @SerializedName("total_data")
    @Expose
    private Integer totalData;
    @SerializedName("data")
    @Expose
    private List<Datum> data = new ArrayList<Datum>();

    /**
     * 
     * @return
     *     The response
     */
    public Response getResponse() {
        return response;
    }

    /**
     * 
     * @param response
     *     The response
     */
    public void setResponse(Response response) {
        this.response = response;
    }

    /**
     * 
     * @return
     *     The filter
     */
    public Filter getFilter() {
        return filter;
    }

    /**
     * 
     * @param filter
     *     The Filter
     */
    public void setFilter(Filter filter) {
        this.filter = filter;
    }

    /**
     * 
     * @return
     *     The totalData
     */
    public Integer getTotalData() {
        return totalData;
    }

    /**
     * 
     * @param totalData
     *     The total_data
     */
    public void setTotalData(Integer totalData) {
        this.totalData = totalData;
    }

    /**
     * 
     * @return
     *     The data
     */
    public List<Datum> getData() {
        return data;
    }

    /**
     * 
     * @param data
     *     The data
     */
    public void setData(List<Datum> data) {
        this.data = data;
    }

}
