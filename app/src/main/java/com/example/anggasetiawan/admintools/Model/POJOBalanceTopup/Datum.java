
package com.example.anggasetiawan.admintools.Model.POJOBalanceTopup;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Datum {

    @SerializedName("current_balance")
    @Expose
    private Integer currentBalance;

    /**
     * 
     * @return
     *     The currentBalance
     */
    public Integer getCurrentBalance() {
        return currentBalance;
    }

    /**
     * 
     * @param currentBalance
     *     The current_balance
     */
    public void setCurrentBalance(Integer currentBalance) {
        this.currentBalance = currentBalance;
    }

}
