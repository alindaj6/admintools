package com.example.anggasetiawan.admintools;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.anggasetiawan.admintools.Body.BalanceInfo;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InfoBalance extends AppCompatActivity {

    @BindView(R.id.infoBalcbUrl)
    CheckBox infoBalUrl;

    @BindView(R.id.etPhoneInfoBal)
    EditText phoneInfoBal;

    @BindView(R.id.btnInfoBal)
    Button infoBal;

    @BindView(R.id.tvPhone)
    TextView phone;

    @BindView(R.id.tvBalance)
    TextView balance;

    private ProgressDialog dialog;
    private NetworkMethodRetrofit2 retrofit2 = new NetworkMethodRetrofit2();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_balance);
        ButterKnife.bind(this);

        infoBal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (infoBalUrl.isChecked()){
                    getUserBalance(CommonConstants.INTERNAL_API_TOKEN, CommonConstants.INTERNAL_URL, phoneInfoBal.getText().toString().trim());
                } else if (!infoBalUrl.isChecked()){
                    getUserBalance(CommonConstants.DEV_API_TOKEN, CommonConstants.DEV_URL, phoneInfoBal.getText().toString().trim());
                }

            }
        });
    }

    private void getUserBalance(String token, String url, final String handphone) {

        showProgressDialog("Processing...");

        final BalanceInfo balanceInfo = new BalanceInfo(token, handphone);

        BalanceAPI service = retrofit2.getRetrofit2(url).create(BalanceAPI.class);

        Call<com.example.anggasetiawan.admintools.Model.POJOBalanceInfo.BalanceInfo> call = service.getCurrBalance(balanceInfo);

        call.enqueue(new Callback<com.example.anggasetiawan.admintools.Model.POJOBalanceInfo.BalanceInfo>() {
            @Override
            public void onResponse(Call<com.example.anggasetiawan.admintools.Model.POJOBalanceInfo.BalanceInfo> call, Response<com.example.anggasetiawan.admintools.Model.POJOBalanceInfo.BalanceInfo> response) {

                if (response.body().getResponse().getCode().equals("200")){
                    phone.setText(handphone);
                    balance.setText(response.body().getData().get(0).getCurrentBalance());
                }
                dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<com.example.anggasetiawan.admintools.Model.POJOBalanceInfo.BalanceInfo> call, Throwable t) {
                dismissProgressDialog();
            }
        });

    }

    public void showProgressDialog(String message){
        if (dialog == null)
            dialog = new ProgressDialog(this);
        if (dialog.isShowing())
            updateProgressDialog(message);
        else {
            dialog.setMessage(message);
            dialog.setCancelable(false);
            dialog.show();
        }
    }

    public void updateProgressDialog(final String message){
        if (dialog.isShowing()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    dialog.setMessage(message);
                }
            });
        }
    }

    public void dismissProgressDialog(){
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
    }

    public void showMessage(String message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
