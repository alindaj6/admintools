
package com.example.anggasetiawan.admintools.Model.POJOMemberDetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Datum {

    @SerializedName("member_name")
    @Expose
    private String memberName;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("register_date")
    @Expose
    private String registerDate;
    @SerializedName("default_locker_name")
    @Expose
    private String defaultLockerName;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("id_card_image")
    @Expose
    private String idCardImage;
    @SerializedName("id_card_validate_status")
    @Expose
    private String idCardValidateStatus;

    /**
     * 
     * @return
     *     The memberName
     */
    public String getMemberName() {
        return memberName;
    }

    /**
     * 
     * @param memberName
     *     The member_name
     */
    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    /**
     * 
     * @return
     *     The phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * 
     * @param phone
     *     The phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * 
     * @return
     *     The email
     */
    public String getEmail() {
        return email;
    }

    /**
     * 
     * @param email
     *     The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 
     * @return
     *     The address
     */
    public String getAddress() {
        return address;
    }

    /**
     * 
     * @param address
     *     The address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * 
     * @return
     *     The registerDate
     */
    public String getRegisterDate() {
        return registerDate;
    }

    /**
     * 
     * @param registerDate
     *     The register_date
     */
    public void setRegisterDate(String registerDate) {
        this.registerDate = registerDate;
    }

    /**
     * 
     * @return
     *     The defaultLockerName
     */
    public String getDefaultLockerName() {
        return defaultLockerName;
    }

    /**
     * 
     * @param defaultLockerName
     *     The default_locker_name
     */
    public void setDefaultLockerName(String defaultLockerName) {
        this.defaultLockerName = defaultLockerName;
    }

    /**
     * 
     * @return
     *     The latitude
     */
    public String getLatitude() {
        return latitude;
    }

    /**
     * 
     * @param latitude
     *     The latitude
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    /**
     * 
     * @return
     *     The longitude
     */
    public String getLongitude() {
        return longitude;
    }

    /**
     * 
     * @param longitude
     *     The longitude
     */
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    /**
     * 
     * @return
     *     The idCardImage
     */
    public String getIdCardImage() {
        return idCardImage;
    }

    /**
     * 
     * @param idCardImage
     *     The id_card_image
     */
    public void setIdCardImage(String idCardImage) {
        this.idCardImage = idCardImage;
    }

    /**
     * 
     * @return
     *     The idCardValidateStatus
     */
    public String getIdCardValidateStatus() {
        return idCardValidateStatus;
    }

    /**
     * 
     * @param idCardValidateStatus
     *     The id_card_validate_status
     */
    public void setIdCardValidateStatus(String idCardValidateStatus) {
        this.idCardValidateStatus = idCardValidateStatus;
    }

}
