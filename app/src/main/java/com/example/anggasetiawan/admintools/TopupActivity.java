package com.example.anggasetiawan.admintools;

import android.app.ProgressDialog;
import android.content.Intent;
import android.opengl.EGLDisplay;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.anggasetiawan.admintools.Body.BalanceTopUp;
import com.example.anggasetiawan.admintools.Model.POJOBalanceTopup.Balancetopup;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TopupActivity extends AppCompatActivity {

    @BindView(R.id.btnTopup)
    Button tombol;

    @BindView(R.id.topUpcbUrl)
    CheckBox cbUrl;

    @BindView(R.id.etAmount)
    EditText amount;

    @BindView(R.id.etPhone)
    EditText phoneMember;

    @BindView(R.id.tvCurrBal)
    TextView currBal;

    private ProgressDialog dialog;
    private NetworkMethodRetrofit2 retrofit2 = new NetworkMethodRetrofit2();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topup);
        ButterKnife.bind(this);

        tombol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (cbUrl.isChecked()){
                    balanceTopUp(CommonConstants.INTERNAL_API_TOKEN, CommonConstants.INTERNAL_URL, phoneMember.getText().toString(), Integer.parseInt(amount.getText().toString()));
                } else if (!cbUrl.isChecked()){
                    balanceTopUp(CommonConstants.DEV_API_TOKEN, CommonConstants.DEV_URL, phoneMember.getText().toString(), Integer.parseInt(amount.getText().toString()));
                }
            }
        });
    }

    private void balanceTopUp (String token, String url, String handphone, Integer amount) {

        showProgressDialog("Processing...");

        BalanceTopUp balanceTopUp = new BalanceTopUp(token, handphone, amount);

        BalanceAPI service = retrofit2.getRetrofit2(url).create(BalanceAPI.class);

        Call<Balancetopup> call = service.getBalanceTopup(balanceTopUp);

        call.enqueue(new Callback<Balancetopup>() {
            @Override
            public void onResponse(Call<Balancetopup> call, Response<Balancetopup> response) {

                if (response.body().getResponse().getCode().equals("200")){

                    currBal.setText(String.valueOf(response.body().getData().get(0).getCurrentBalance()));
                    Toast.makeText(getApplicationContext(), response.body().getResponse().getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), response.body().getResponse().getMessage(), Toast.LENGTH_SHORT).show();
                }
                dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<Balancetopup> call, Throwable t) {
                dismissProgressDialog();
            }
        });
    }

    public void showProgressDialog(String message){
        if (dialog == null)
            dialog = new ProgressDialog(this);
        if (dialog.isShowing())
            updateProgressDialog(message);
        else {
            dialog.setMessage(message);
            dialog.setCancelable(false);
            dialog.show();
        }
    }

    public void updateProgressDialog(final String message){
        if (dialog.isShowing()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    dialog.setMessage(message);
                }
            });
        }
    }

    public void dismissProgressDialog(){
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
    }

    public void showMessage(String message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
