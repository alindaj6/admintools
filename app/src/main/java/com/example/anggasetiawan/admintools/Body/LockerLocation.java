package com.example.anggasetiawan.admintools.Body;

/**
 * Created by user on 10/27/16.
 */

public class LockerLocation {

    String token;
    String country;
    String province;
    String city;

    public LockerLocation(String token, String country, String province, String city) {

        this.token = token;
        this.country = country;
        this.province = province;
        this.city = city;
    }
}
