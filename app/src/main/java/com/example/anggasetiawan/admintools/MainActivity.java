package com.example.anggasetiawan.admintools;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.btnlatLngCheckMain)
    Button LatLngCheckMain;
    @BindView(R.id.btnMemberMain)
    Button memberDetailMain;
    @BindView(R.id.btnTopupMain)
    Button topUpMain;
    @BindView(R.id.btnFreeBalanceMain)
    Button freeBalanceMain;
    @BindView(R.id.btnBalanceInfoMain)
    Button balanceInfoMain;

    private String[] arraySpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        //this.arraySpinner = new String[] {"Internal API", "Developer API"};

        LatLngCheckMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), LatLngCheck.class));
            }
        });

        memberDetailMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), MemberDetail.class));
            }
        });

        balanceInfoMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), InfoBalance.class));
            }
        });

        freeBalanceMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), FreeBalanceCheck.class));
            }
        });

        topUpMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), TopupActivity.class));
            }
        });

    }
}
