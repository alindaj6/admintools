
package com.example.anggasetiawan.admintools.Model.POJOBalanceBonus;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Balancebonus {

    @SerializedName("response")
    @Expose
    private Response response;
    @SerializedName("bonus_receiver")
    @Expose
    private String bonusReceiver;
    @SerializedName("bonus_amount")
    @Expose
    private String bonusAmount;

    /**
     * 
     * @return
     *     The response
     */
    public Response getResponse() {
        return response;
    }

    /**
     * 
     * @param response
     *     The response
     */
    public void setResponse(Response response) {
        this.response = response;
    }

    /**
     * 
     * @return
     *     The bonusReceiver
     */
    public String getBonusReceiver() {
        return bonusReceiver;
    }

    /**
     * 
     * @param bonusReceiver
     *     The bonus_receiver
     */
    public void setBonusReceiver(String bonusReceiver) {
        this.bonusReceiver = bonusReceiver;
    }

    /**
     * 
     * @return
     *     The bonusAmount
     */
    public String getBonusAmount() {
        return bonusAmount;
    }

    /**
     * 
     * @param bonusAmount
     *     The bonus_amount
     */
    public void setBonusAmount(String bonusAmount) {
        this.bonusAmount = bonusAmount;
    }

}
