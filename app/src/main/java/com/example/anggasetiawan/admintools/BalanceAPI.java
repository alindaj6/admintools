package com.example.anggasetiawan.admintools;


import com.example.anggasetiawan.admintools.Body.BalanceBonus;
import com.example.anggasetiawan.admintools.Body.BalanceInfo;
import com.example.anggasetiawan.admintools.Body.BalanceTopUp;
import com.example.anggasetiawan.admintools.Model.POJOBalanceBonus.Balancebonus;
import com.example.anggasetiawan.admintools.Model.POJOBalanceTopup.Balancetopup;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by user on 11/11/16.
 */

public interface BalanceAPI {

    @POST(CommonConstants.URL_BALANCE_INFO)
    @Headers({CommonConstants.HEADER_CONTENT_TYPE_NAME + CommonConstants.HEADER_CONTENT_TYPE_VALUE})
    Call<com.example.anggasetiawan.admintools.Model.POJOBalanceInfo.BalanceInfo> getCurrBalance(@Body BalanceInfo balanceInfo);

    @POST(CommonConstants.URL_BALANCE_BONUS)
    @Headers({CommonConstants.HEADER_CONTENT_TYPE_NAME + CommonConstants.HEADER_CONTENT_TYPE_VALUE})
    Call<Balancebonus> getBalanceBonus(@Body BalanceBonus balanceBonus);

    @POST(CommonConstants.URL_BALANCE_TOPUP)
    @Headers({CommonConstants.HEADER_CONTENT_TYPE_NAME + CommonConstants.HEADER_CONTENT_TYPE_VALUE})
    Call<Balancetopup> getBalanceTopup(@Body BalanceTopUp balanceTopUp);

}
