package com.example.anggasetiawan.admintools;

import com.example.anggasetiawan.admintools.Body.*;
import com.example.anggasetiawan.admintools.Model.POJOMemberDetail.Memberdetail;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by user on 11/11/16.
 */

public interface MemberDataAPI {

    @POST(CommonConstants.URL_MEMBER_DETAIL)
    @Headers({CommonConstants.HEADER_CONTENT_TYPE_NAME + CommonConstants.HEADER_CONTENT_TYPE_VALUE})
    Call<Memberdetail> getMemberDetail(@Body com.example.anggasetiawan.admintools.Body.MemberDetail memberDetail);

}
