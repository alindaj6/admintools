
package com.example.anggasetiawan.admintools.Model.POJOBalanceInfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Datum {

    @SerializedName("current_balance")
    @Expose
    private String currentBalance;

    /**
     * 
     * @return
     *     The currentBalance
     */
    public String getCurrentBalance() {
        return currentBalance;
    }

    /**
     * 
     * @param currentBalance
     *     The current_balance
     */
    public void setCurrentBalance(String currentBalance) {
        this.currentBalance = currentBalance;
    }

}
