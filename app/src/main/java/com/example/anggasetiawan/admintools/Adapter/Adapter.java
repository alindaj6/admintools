package com.example.anggasetiawan.admintools.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.anggasetiawan.admintools.BrokenLatLng;
import com.example.anggasetiawan.admintools.Model.POJOLockerLocation.Datum;
import com.example.anggasetiawan.admintools.R;

import java.util.List;

/**
 * Created by anggasetiawan on 11/28/16.
 */

public class Adapter extends RecyclerView.Adapter <Adapter.MyViewHolder> {

    List<BrokenLatLng> brokenLatLngList;
    Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView lockerName, lockerLat, lockerLng;


        public MyViewHolder(View view) {
            super(view);

            lockerName = (TextView) view.findViewById(R.id.tvNameLocker);
            lockerLat = (TextView) view.findViewById(R.id.tvLatLocker);
            lockerLng = (TextView) view.findViewById(R.id.tvLngLocker);

        }
    }

    public Adapter (Context context, List<BrokenLatLng> brokenLatLngList){
        this.context = context;
        this.brokenLatLngList = brokenLatLngList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_latlng_check, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        BrokenLatLng data = brokenLatLngList.get(position);

        holder.lockerName.setText(data.getName());
        holder.lockerLat.setText(data.getLatitude().replace(" ", ""));
        holder.lockerLng.setText(data.getLongitude().replace(" ", ""));

    }

    @Override
    public int getItemCount() {
        return brokenLatLngList.size();
    }
}
