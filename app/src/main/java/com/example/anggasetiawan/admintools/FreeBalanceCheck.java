package com.example.anggasetiawan.admintools;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.anggasetiawan.admintools.Body.BalanceBonus;
import com.example.anggasetiawan.admintools.Model.POJOBalanceBonus.Balancebonus;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FreeBalanceCheck extends AppCompatActivity {

    @BindView(R.id.freeBalcbUrl)
    CheckBox freeBalUrl;

    @BindView(R.id.btnCheckFreeBal)
    Button checkFreeBal;

    @BindView(R.id.etFreeBalPhone)
    EditText freeBalPhone;

    @BindView(R.id.tvFreeBal)
    TextView freeBal;

    @BindView(R.id.tvExpired)
    TextView expired;

    private ProgressDialog dialog;
    private NetworkMethodRetrofit2 retrofit2 = new NetworkMethodRetrofit2();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_free_balance_check);
        ButterKnife.bind(this);

        checkFreeBal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (freeBalUrl.isChecked()){
                    balanceBonus(CommonConstants.INTERNAL_API_TOKEN, CommonConstants.INTERNAL_URL, freeBalPhone.getText().toString().trim());
                } else if (!freeBalUrl.isChecked()){
                    balanceBonus(CommonConstants.DEV_API_TOKEN, CommonConstants.DEV_URL, freeBalPhone.getText().toString().trim());
                }

            }
        });
    }

    private void balanceBonus(String token, String url, String handphone){

        showProgressDialog("Processing...");

        BalanceBonus balanceBonus = new BalanceBonus(token, handphone);

        BalanceAPI service = retrofit2.getRetrofit2(url).create(BalanceAPI.class);

        Call<Balancebonus> call = service.getBalanceBonus(balanceBonus);

        call.enqueue(new Callback<Balancebonus>() {
            @Override
            public void onResponse(Call<Balancebonus> call, Response<Balancebonus> response) {

                if (response.body().getResponse().getCode().equals("200")){

                    freeBal.setText(response.body().getBonusAmount());
                    //expired.setText(response.body().getBonusReceiver());

                } else {
                    freeBal.setText(response.body().getResponse().getMessage());
                }
                dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<Balancebonus> call, Throwable t) {
                dismissProgressDialog();
            }
        });
    }

    public void showProgressDialog(String message){
        if (dialog == null)
            dialog = new ProgressDialog(this);
        if (dialog.isShowing())
            updateProgressDialog(message);
        else {
            dialog.setMessage(message);
            dialog.setCancelable(false);
            dialog.show();
        }
    }

    public void updateProgressDialog(final String message){
        if (dialog.isShowing()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    dialog.setMessage(message);
                }
            });
        }
    }

    public void dismissProgressDialog(){
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
    }

    public void showMessage(String message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
