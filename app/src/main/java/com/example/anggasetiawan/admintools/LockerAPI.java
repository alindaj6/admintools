package com.example.anggasetiawan.admintools;


import com.example.anggasetiawan.admintools.Body.LockerLocation;
import com.example.anggasetiawan.admintools.Model.POJOLockerLocation.Lockerlocation;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by user on 11/11/16.
 */

public interface LockerAPI {

    @POST(CommonConstants.URL_LOCKER_LOCATION)
    @Headers({CommonConstants.HEADER_CONTENT_TYPE_NAME + CommonConstants.HEADER_CONTENT_TYPE_VALUE})
    Call<Lockerlocation> getLockerLocation(@Body LockerLocation lockerLocation);

}
