package com.example.anggasetiawan.admintools;

/**
 * Created by popbox.asia devteam 2016 on 6/23/2016.
 *
 */
public class CommonConstants {

    public static final String PREF_NAME = "pop-box-pref";

    public static final String DEV_URL = "http://api-dev.popbox.asia";
    public static final String DEV_API_TOKEN = "e5edf260d46e99ad3c612e6a2778d172281403fb"; // Update Rev : 6 Sept 2016

    public static final String INTERNAL_URL = "https://internalapi.popbox.asia";
    public static final String INTERNAL_API_TOKEN = "5701d8ef69b37f1a3210eed1df3b5ba0f6b0a30a"; // Update Rev : 3 Nov 2016

    public static final String PARTNER_URL = "https://partnerapi.popbox.asia";
    public static final String PARTNERAPI_TOKEN = "4c6eb7c205db97096cdb27a0c4f1fff4e335d95f";

    //public static final String HEADER_CONTENT_TYPE_NAME = "ContentType:";
    public static final String HEADER_CONTENT_TYPE_NAME = "Content-Type:";
    public static final String HEADER_CONTENT_TYPE_VALUE = "application/json";

    //Api token untuk Prodcution Masih yang lama per september 2016
    //public static final String API_TOKEN = "14d5b792dd12150bf5b8a5d6a9a7e583650d37af"; //Old Token Last Use : 06SEPT2016

    public static final String RECEIVER_MAIN_ACTIVITY = "mainactivity";
    public static final String RECEIVER_NOTIFICATION_ACTIVITY = "notificationactivity";

    //API Endpoint User
    public static final String URL_MEMBER_REGISTER = "/member/register";
    public static final String URL_MEMBER_VALIDATION = "/member/validation";
    public static final String URL_MEMBER_RESENDOTP = "/member/resendotp";
    public static final String URL_MEMBER_UPDATE = "/member/update";
    public static final String URL_MEMBER_DETAIL = "/member/detail";
    public static final String URL_MEMBER_LOGIN = "/member/login";
    public static final String URL_MEMBER_CREATEPWD = "/member/createpwd";
    public static final String URL_MEMBER_UPDATEPWD = "/member/updatepwd";
    public static final String URL_MEMBER_IDUPLOAD = "/member/idupload";

    public static final String URL_TRACKING_LIST = "/tracking/list";
    public static final String URL_TRACKING_DETAIL = "/tracking/detail";

    //API Endpoint Locker Location
    public static final String URL_NEAREST_LIST = "/nearest/list";
    public static final String URL_LOCKER_SEARCH = "/locker/search";
    public static final String URL_LOCKER_LOCATION = "/locker/location"; //Display All PopBox Locker.

    public static final String URL_PRODUCT_CATEGORY = "/product/category";
    public static final String URL_PRODUCT_LIST = "/product/list";
    public static final String URL_PRODUCT_LISTCAT = "/product/listcat";
    public static final String URL_PRODUCT_DETAIL = "/product/detail?sku=";

    //API Endpoint Balance
    public static final String URL_BALANCE_INFO = "/balance/info";
    public static final String URL_BALANCE_DEDUCT = "/balance/deduct";
    public static final String URL_BALANCE_HISTORY = "/balance/history";
    public static final String URL_BALANCE_TOPUP = "/balance/topup"; //TODO : diredirect ke api-dev
    public static final String URL_BALANCE_BONUS = "/balance/bonus";  //TODO : diredirect ke api-dev

    //API Endpoint Order
    public static final String URL_ORDER_SUBMIT = "/order/submit";
    public static final String URL_ORDER_HISTORY = "/order/history";
    public static final String URL_ORDER_DETAIL = "/order/detail";
    public static final String URL_ORDER_INVOICE = "/order/invoice";

    //API Endpoint Payment
    public static final String URL_PAYMENT_TRANSF = "/payment/transf";
    public static final String URL_PAYMENT_CONFIRM = "/payment/confirm";
    public static final String URL_PAYMENT_MOBILE = "/payment/mobile";

    //API Endpoint Pickup from Locker
    public static final String URL_PICKUP_HISTORY = "/pickup/history";
    public static final String URL_PICKUP_SUBMIT = "/pickup/submit";
    public static final String URL_PICKUP_DETAIL = "/pickup/detail";
    public static final String URL_PICKUP_CALC = "/pickup/calc";

    //API Endpoint Parcel
    public static final String URL_PARCEL_STATUS = "/parcel/status";

    public static final String URL_O2O_HISTORY = "/oto/history";

    public static final String API_DATE_FORMAT = "yyyy-MM-dd hh:mm:ss";
    public static final String DEFAULT_DATE_FORMAT = "MMM dd, yyyy";
    public static final String DATABASE_NAME = "popbox.db";
    public static final int DATABASE_VERSION = 1;

    public static final int NOTIFICATION_ID = 13;

    public static final String MyPREFERENCES = "PopBoxPrefs";
    public static final String MyPREFERENCES_PHONE = "phone";
    public static final String MyPREFERENCES_UIDTOKEN = "uidtoken";
    public static final String MyPREFERENCES_MEMBERNAME = "memberName";
    public static final String MyPREFERENCES_EMAIL = "email";
    public static final String MyPREFERENCES_ADDRESS = "address";
    public static final String MyPREFERENCES_DEFAUTLOCKER = "defaultLocker";
    public static final String MyPREFERENCES_USERLAT = "userLat";
    public static final String MyPREFERENCES_USERLNG = "userLng";
    public static final String MyPREFERENCES_USERPHOTO = "userPhoto";
    public static final String MyPREFERENCES_USERBALANCE = "userBalance";
    public static final String MyPREFERENCES_FCMTOKEN = "fcmToken";
    public static final String MyPREFERENCES_INVPAYMENT = "invpayment";

    public static final String MyPREFERENCES_DATA = "PopBoxData";
    public static final String MyPREFERENCES_DATALOCKER_NAME = "LockerName";
    public static final String MyPREFERENCES_DATALOCKER_ADDRESS = "LockerAddress";
    public static final String MyPREFERENCES_DATALOCKER_ADDRESS2 = "LockerAddress2";
    public static final String MyPREFERENCES_DATA_OPERATIONAL = "OperationalHours";
    public static final String MyPREFERENCES_DATA_INVOICE = "InvoiceID";
    public static final String MyPREFERENCES_DATA_LAT = "Latitude";
    public static final String MyPREFERENCES_DATA_LNG = "Longitude";
    public static final String MyPREFERENCES_DATA_RCPLAT = "Longitude";
    public static final String MyPREFERENCES_DATA_RCPLNG = "Longitude";
    public static final String MyPREFERENCES_DATA_IMAGE1 = "Longitude";
    public static final String MyPREFERENCES_DATA_IMAGE2 = "Longitude";
    public static final String MyPREFERENCES_DATA_IMAGE3 = "Longitude";

    public static final String INTENT_TAG_HANDPHONE = "handphone";
    public static final String INTENT_TAG_SESSIONID = "sessionid";
    public static final String INTENT_TAG_REQUIRESTEP3 = "requirestep3";
    public static final String INTENT_TAG_LOCKER_NAME = "lockerName";
    public static final String INTENT_TAG_LOCKER_LATITUDE = "lockerLatitude";
    public static final String INTENT_TAG_LOCKER_LONGITUDE = "lockerLongitude";
    public static final String INTENT_TAG_SELECTED_LOCKER = "selectedLocker";
    public static final String INTENT_TAG_PRODUCT = "product";
    public static final String INTENT_TAG_CONTINUESHOP = "continue_shop";
    public static final String INTENT_TAG_LOCKER_NEXT_CLASS = "locker_next_class";
    public static final String INTENT_TAG_USER_DETAIL = "user_detail";
    public static final String INTENT_TAG_TO_POPBOX = "to_popbox";
    public static final String INTENT_TAG_TO_PICKUP = "to_pickup";
    public static final String INTENT_TAG_HISTORY_TYPE = "history_title";
    public static final String INTENT_TAG_HISTORY_LIST = "history_list";
    public static final String INTENT_TAG_HISTORY_OBJECT = "history_object";
    public static final String INTENT_TAG_BANK = "bank_object";
    public static final String INTENT_TAG_INVOICE = "invoice";
    public static final String INTENT_TAG_SOURCE = "source";
    public static final String INTENT_TAG_KTP_CAPTURED = "ktp_captured";
    public static final String INTENT_TAG_LOCKER_SELECTED = "locker_selected";
    public static final String INTENT_TAG_PICKUP_MODE = "pickup_mode";
    public static final String INTENT_TAG_LOCKER_SIZE = "locker_size";
    public static final String INTENT_TAG_PICKUP = "pickup";
    public static final String INTENT_TAG_WEB_URL = "web_url";
    public static final String INTENT_TAG_BALANCES = "balance";
    public static final String INTENT_TAG_NOTIFICATION_MESSAGE = "noticationmessage"; //TODO : noticationmessage change the typo
    public static final String INTENT_TAG_NOTIFICATION = "notification";
    public static final String INTENT_TAG_SEARCH_TYPE = "searchtype";
    public static final String INTENT_TAG_POPSEND_STEP = "step1";

    // FOR WHEN FIRST REGISTRATION
    public static final String INTENT_TAG_MEMBER_NAME = "memberName";
    public static final String INTENT_TAG_MEMBER_PHONE = "memberPhone";
    public static final String INTENT_TAG_MEMBER_EMAIL = "memberEmail";
    public static final String INTENT_TAG_MEMBER_PASS1 = "memberPass1";
    public static final String INTENT_TAG_MEMBER_PASS2 = "memberPass2";

    // DATA MAPS ACTIVITY FOR RECIPIENT ADDRESS
    public static final String INTENT_TAG_ADDRESS = "address";
    public static final String INTENT_TAG_ADDRESS_NOL = "address_nol";
    public static final String INTENT_TAG_ADDRESS_SATUDUA = "address_satudua";
    public static final String INTENT_TAG_ADDRESS_LOCLAT = "address_loclat";
    public static final String INTENT_TAG_ADDRESS_LOCLNG = "address_loclng";
    public static final String INTENT_TAG_LAST_ADDRESS_LATLNG = "last_address_latlng";

    public static final String INTENT_TAG_ACTIVITY_NAME = "activity_name";
    public static final String INTENT_TAG_FROM_MAIN = "MainActivity";

    public static final String INTENT_TAG_FROM_PICKUP = "SourceActivity";
    //public static final String INTENT_TAG_FROM_MYPOPBOX = "RecipientActivity";
    //public static final String INTENT_TAG_FROM_MYPOPBOX = "ConfirmationActivity";
    //public static final String INTENT_TAG_FROM_MYPOPBOX = "ThankYouPage";
    //public static final String INTENT_TAG_FROM_MYPOPBOX = "PopSendConfirmed";

    //public static final String INTENT_TAG_FROM_MYPOPBOX = "MyPopBox";
    public static final String INTENT_TAG_FROM_PROFLE = "Profile";
    //public static final String INTENT_TAG_FROM_MYPOPBOX = "MyBalance";
    //public static final String INTENT_TAG_FROM_MYPOPBOX = "FavLocker";
    public static final String INTENT_TAG_FROM_FAREESTIMATE = "FareEstimate";
    //public static final String INTENT_TAG_FROM_MYPOPBOX = "ChangePass";
    public static final String INTENT_TAG_FROM_MYPOPBOX = "Logout";

    //public static final String INTENT_TAG_FROM_HISTORY = "HistoryActivity2";
    public static final String INTENT_TAG_FROM_HISTORY = "FAQ";

    public static final int ACTIVITY_REGISTER = 111;
    public static final int ACTIVITY_LOCKER = 112;
    public static final int ACTIVITY_SEARCH_LOCKER = 113;
    public static final int ACTIVITY_IMAGE_CHOOSER = 114;
    public static final int ACTIVITY_IMAGE_CAPTURE = 115;
    public static final int ACTIVITY_PRODUCT_DETAIL = 116;
    public static final int ACTIVITY_SHOPPING_CART = 117;
    public static final int ACTIVITY_PENDING_PAYMENT = 118;
    public static final int ACTIVITY_LOCKER_SIZE = 119;

    public static final String SOURCE_MYPOPBOX = "mypopbox";
    public static final String SOURCE_POPSHOP = "popshop";
    public static final String SOURCE_PICKUP = "pickup";

    public static final String URL_FAQ = "http://www.popbox.asia/faq";
    public static final String URL_TNC = "http://www.popbox.asia/company/terms_and_conditions";
    public static final String URL_FEEDBACK = "http://www.popbox.asia/company/contact_us";

    public static final String POPSHOP_BELUM_BAYAR = "Belum dibayar";
    public static final String PROFILE_KTP_NOTFOUND = "http://api-dev.popbox.asia/img/member/notfound.png";

    public static final boolean isProduction = false;
    public static final boolean byPassOTP = false;

    public static final String ITEM_SIZE_DOCUMENT = "Document";
    public static final String ITEM_SIZE_SMALL = "Small Parcell";
    public static final String ITEM_SIZE_MEDIUM = "Medium Parcell";

    //Semakin Kecil nilai semakin jauh.
    public static final int ZOOM_MAP_BUILDING = 20; //closer
    public static final int ZOOM_MAP_STREET = 14; //streetView
    public static final int ZOOM_MAP_CITY = 10; //farView

    public static final String PREFIX_TOPUP_BALANCE = "-BAL-";

    //doku development
    /*public static final String DOKU_SECRET_KEY = "fh7Au8FwUL73";
    public static final String DOKU_MERCHANT_ID = "3443";
    public static final String DOKU_CURRENCY = "360";
    public static final String DOKU_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArstteWwTGAMSjYhCV24oGqm7fBIxJP5L7v7qnoUHU8SUqRCRrCyC6GHAumG+TU+BhSWdE6BGaCnJM7I1el08fFeUGsqoe0g6TlcVwCTaJYZjZQkQEwExibsYvT7405PlyB/PQwwbfS6+2EF8EjKNVJKskcW6nLSmQFLpy1sxGNfl8c0iqWMbU9xzlBBG8h/bnnBGzeq8opKnmiNUTUKaGtzZEHP6P0odyWZFG/C8vxkuxSi7TmMygwmNYtyqzsgjBvgSJzcyCUfl7j4expTUFYawtdb0s4H3a239qtI/YNKy7LtFN8NkWVIpUCqB2TFNwCjgarK73K56IwbsPyWOzQIDAQAB";
    public static final String CHAIN_MERCHANT ="NA";
    */

    //doku production
    public static final String DOKU_SECRET_KEY = "fh7Au8FwUL73";
    public static final String DOKU_MERCHANT_ID = "1554";
    public static final String DOKU_CURRENCY = "360";
    public static final String DOKU_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmmPDxHO4a1Q2d83gXqaeE4wngboHsRXyN5upZH66O+Rk108JIL/97iijWJyxFCGw1JnJ77Zt3il8JAxor4mO9R0Gkx85b3YZ1l18R7K+43MT2jsTXO4U1vXAOGTLH38ntEgT3D93GMCoLsYlmo6jV3fyTeVOwUWVytvTf4VwheX26wLmVyCAUI9FWRiNcfA5jjqXWsa37OSa38GJUqXO2tJmf0pSUyuDk4FDGzvoIba4/aLgoTk3aaldkCxGyUGaNKjOs+hCGgnI/qctb7dqhBIL2ZJLXM8Qv8UP5qMTRQdknMaxVs1a3Ba+Dpf8kdhVaEJTyfxSWL4qNAlEJzrm8wIDAQAB";
    public static final boolean DOKU_PRODUCTION_STATUS = true;
    public static final String DOKU_CHAIN_MERCHANT ="1";

    public static final String ACTIVE_REGION_INA ="indonesia";
    public static final String ACTIVE_REGION_MY ="malaysia";

    //change if your have to change region
    public static final String ACTIVE_REGION = ACTIVE_REGION_INA;
    public static final String GPLACE_COUNTRY ="ID";
}

